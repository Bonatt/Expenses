import sys; sys.path.append('/home/joshua/Other/FunCode'); from ROOTStyleEtc import *





### A log of expenses starting October 31, 2017. See parent Expenses.ods file.






import pandas as pd

file = 'Expenses.csv'
# Raw dataframe
df = pd.read_csv(file)
# Presumably the interesting data of df
df2 = df.groupby(['TYPE','SUBTYPE','SUBSUBTYPE','NAME'], as_index=False).sum()

df2v = df2.values #[list(i) for i in df2.values]















df2v0 = df2v[0]


t,st,sst,n,p = df2v0


import collections

#d = defaultdict(dict)

#df2v0 = df2v[0]


d = collections.defaultdict(dict)
for t,st,sst,n,p in df2v:
    d[t] = {}
    d[t][st] = {}
    d[t][st][sst] = {}
    if 
    d[t][st][sst][n] = p






from collections import defaultdict
nested_dict = lambda: defaultdict(nested_dict)
nest = nested_dict()
for t,st,sst,n,p in df2v:
    nest[t][st][sst][n] = p






from collections import defaultdict

d = defaultdict(dict)
d['dict1']['innerkey'] = 'value'
d  # currently a defaultdict type
dict(d)  # but is exactly like a normal dictionary.











# t,st,sst,n
from collections import defaultdict
d = defaultdict( lambda: defaultdict( lambda: defaultdict( lambda: defaultdict(list) ) ) )
for t,st,sst,n,p in df2v:
    d[t][st][sst][n].append(p)















### In order that df2 prints:

name = df2['NAME'].values
namesum = df2.groupby(['TYPE','SUBTYPE','SUBSUBTYPE','NAME']).sum().values)


TYPE = ()
SUBTYPE = [(i,j[0]) for i,j in zip(sorted(set(df2['SUBTYPE'].values)),df2.groupby(['TYPE','SUBTYPE']).sum().values)]
SUBSUBTYPE = [(i,j[0]) for i,j in zip(set(df2['SUBSUBTYPE'].values),df2.groupby(['TYPE','SUBTYPE','SUBSUBTYPE']).sum().values)]

NAME = [(i,j[0]) for i,j in zip(name, namesum]




# Price by type. These are all alphabetical...
type = set([i for i in df2['TYPE'].values])
cost_type = [i[0] for i in df2.groupby('TYPE').sum().values]
# Price by subtype
subtype = set([i for i in df2['SUBTYPE'].values])
cost_subtype = [i[0] for i in df2.groupby('SUBTYPE').sum().values]
# Price by subsubtype
subsubtype = set([i for i in df2['SUBSUBTYPE'].values])
cost_subsubtype = [i[0] for i in df2.groupby('SUBSUBTYPE').sum().values]
# Price by name
name = set([i for i in df2['NAME'].values])
cost_name = [i[0] for i in df2.groupby('NAME').sum().values]



### Prices for each category
#TYPE = (list(set(df2['TYPE'].values)),





























header = list(df)
header2 = header[:]
header2.remove('PRICE')
for h in header2:
    print df.groupby(h).sum()
    print ''

# Price by type. These are all alphabetical...
type = sorted(set([i for i in df['TYPE'].values[1:]]))
cost_type = [i[0] for i in df.groupby('TYPE').sum().values]
# Price by subtype
subtype = sorted(set([i for i in df['SUBTYPE'].values[1:]]))
cost_subtype = [i[0] for i in df.groupby('SUBTYPE').sum().values]
# Price by subsubtype
subsubtype = sorted(set([i for i in df['SUBSUBTYPE'].values[1:]]))
cost_subsubtype = [i[0] for i in df.groupby('SUBSUBTYPE').sum().values]
# Price by name
name = sorted(set([i for i in df['NAME'].values[1:]]))
cost_name = [i[0] for i in df.groupby('NAME').sum().values]

costs = [ (type,cost_type),(subtype,cost_subtype), (subsubtype,cost_subsubtype), (name,cost_name) ]


# (type, cost_type), etc.
tct = [(i,j) for i,j in zip(type,cost_type)]
stcst = [(i,j) for i,j in zip(subtype,cost_subtype)]
sstcsst = [(i,j) for i,j in zip(subsubtype,cost_subsubtype)]
ncn = [(i,j) for i,j in zip(name,cost_name)]

costs2 = tct+stcst+sstcsst+ncn


cats = type + subtype + subsubtype + name
costs3 = cost_type + cost_subtype + cost_subsubtype + cost_name





def dstruct(key, value, dstr=None):
    if dstr: return [(key, value, dstr)]#dstruct(dstr))]
    else: return [(key, value, [])]


ndirs = [len(type), len(subtype), len(subsubtype), len(name)]
idirs = [i-1 for i in ndirs]

TYPE = dstruct(costs2[0][0],costs2[0][1])










# Values of df into lists. Strip date, store, person, and header buffer
a = [list(i[2:-1]) for i in df.values[headersize:]]
# Transpose of a
a2 = map(list, zip(*a))
# Uniques of a2
a3 = [list(set(i)) for i in a2]


'''
aa = a[0]
#range(len(aa)-1) = [0, 1, 2, 3]
p = aa[-1]
data = dstruct(aa[0], p, dstruct(aa[1], p, dstruct(aa[2], p, dstruct(aa[3], p))))

for aa in a:
    p = aa[-1]
    dstruct(aa[0], p, dstruct(aa[1], p, dstruct(aa[2], p, dstruct(aa[3], p))))
'''

#dstruct(costs[0][0][0],costs[0][1][0], dstruct('ok2',2))



# Only type, subtype, subsubtype, name, price
header3 = header[2:-1]
# From df, take only header3 columns and drop NaN row ('' row)
df2 = df[header3].sort_values(by=header3)#[:-1]




# Only type, subtype, subsubtype, name
header4 = header[2:-2]



df3 = df.groupby(header4).sum() # == df2.groupby(header3).sum()
df4 = df.groupby(header4, as_index=False).sum()
#df.groupby(header4[::-1]).sum()




# df4 to list. == a but merges both tomatoes, e.g. No duplicate names, now.
b = [list(i) for i in df4.values]
# Uniques of b. Note that this means same prices also get collapsed...
b2 = [list(set(i)) for i in map(list, zip(*b))]

ndirs = [len(i) for i in b2[:-1]]




'''
data = []
for bb in b:
    #t = bb[0]
    #st = bb[1]
    #sst = bb[2]
    #n = bb[3] # name
    #p = bb[4] # price
    t,st,sst,n,p = bb
    #if n not in data[0][2][0][2][0][2][0]:
    data.append(dstruct(t, p, dstruct(st, p, dstruct(sst, p, dstruct(n, p)))))


# data = dstruct(bb[0], p, dstruct(bb[1], p, dstruct(bb[2], p, dstruct(bb[3], p))))
# data[0][2][0][2][0][2][0] = ('Croissant', 4.3100000000000005, []), e.g.
'''



#b0 = b[0]
data = []
for bb in b:
    t,st,sst,n,p = bb
    #tempdata = dstruct(t, p, dstruct(st, p, dstruct(sst, p, dstruct(n, p))))
    #if n not in tempdata[0][2][0][2][0][2][0]:
    #    data.append(dstruct(t, p, dstruct(st, p, dstruct(sst, p, dstruct(n, p)))))
    #else:
    data.append(dstruct(t, p, dstruct(st, p, dstruct(sst, p, dstruct(n, p)))))




'''
data = []
for cc,bb in zip(costs,b):
    tc,stc,sstc,nc = costs
    t,st,sst,n,p = bb
    data.append(dstruct(t, p, dstruct(st, p, dstruct(sst, p, dstruct(n, p)))))
'''






subdir = 2
'''
d0 = data[0][0]
d1 = data[1][0]

d0t = d0[0]
d1t = d1[0]

if d1t == d0t: d0[subdir].append(d1[subdir])
'''

'''
data2 = data[0][0] # == ('Food', 2.0, [('Bakery', 2.0, [('Croissant', 2.0, [('Croissant', 2.0, [])])])])
for i,d in enumerate(data):
    if i == 0: continue # return to top of loop, next i. To start appending at i=1
    else:
        print d[i-1]
        print d
        d0 = d[i-1]
        d1 = d
        d0t = d0[0] # "food"
        d1t = d1[0]
        if d1t == d0t: d0[subdir].append(d1[subdir])

'''
'''
# Initialize new data structure to be nested as first data. Then if there are matches, nest appropriately.
data2 = data[0][0]
for i,row in enumerate(data):
    if i == 0: continue
    else:
        #print i, row
'''

# Yes, I know this is unPythonic.
# Initialize new data structure to be nested as first data. Then if there are type matches, nest appropriately.
data2 = data[0][0]
for i in range(len(data)):
    if i == 0: continue
    else:
        d0 = data[i-1][0]   # i-1 data row
        d0t = d0[0]         # i-1 data row's type
        d1 = data[i][0]   
        d1t = d1[0]
        if d1t == d0t:
            data2[subdir].append(d1[subdir][0])
data2 = [data2]





[dstruct(k,v,[]) for k,v in zip(name,cost_name)]





























### From https://stackoverflow.com/questions/12926779/how-to-make-a-sunburst-plot-in-r-or-python

import matplotlib.pyplot as plt

def sunburst(nodes, total=np.pi * 2, offset=0, level=0, ax=None):
    ax = ax or plt.subplot(111, projection='polar')

    if level == 0 and len(nodes) == 1:
        label, value, subnodes = nodes[0]
        ax.bar([0], [0.5], [np.pi * 2])
        ax.text(0, 0, label, ha='center', va='center')
        sunburst(subnodes, total=value, level=level + 1, ax=ax)
    elif nodes:
        d = np.pi * 2 / total
        labels = []
        widths = []
        local_offset = offset
        for label, value, subnodes in nodes:
            labels.append(label)
            widths.append(value * d)
            sunburst(subnodes, total=total, offset=local_offset,
                     level=level + 1, ax=ax)
            local_offset += value
        values = np.cumsum([offset * d] + widths[:-1])
        heights = [1] * len(nodes)
        bottoms = np.zeros(len(nodes)) + level - 0.5
        rects = ax.bar(values, heights, widths, bottoms, linewidth=1,
                       edgecolor='white', align='edge')
        for rect, label in zip(rects, labels):
            x = rect.get_x() + rect.get_width() / 2
            y = rect.get_y() + rect.get_height() / 2
            rotation = (90 + (360 - np.degrees(x) % 180)) % 360
            ax.text(x, y, label, rotation=rotation, ha='center', va='center') 

    if level == 0:
        ax.set_theta_direction(-1)
        ax.set_theta_zero_location('N')
        ax.set_axis_off()
'''
data0 = [    ('/', 100, [
                ('home', 70, [
                    ('Images', 40, []),
                    ('Videos', 20, []),
                    ('Documents', 5, []), ]),
                ('usr', 15, [
                    ('src', 6, [
                        ('linux-headers', 4, []),
                        ('virtualbox', 1, []), ]),
                    ('lib', 4, []),
                    ('share', 2, []),
                    ('bin', 1, []),
                    ('local', 1, []),
                    ('include', 1, []), ]), ]),     ]

sunburst(data0)
plt.show()
'''




