import sys; sys.path.append('/home/joshua/Other/FunCode'); from ROOTStyleEtc import *





### A log of expenses starting October 31, 2017. See parent Expenses.ods file.






import pandas as pd

file = 'Expenses.csv'
# Raw dataframe
df = pd.read_csv(file)
# Presumably the interesting data of df
df2 = df.groupby(['TYPE','SUBTYPE','SUBSUBTYPE','NAME'], as_index=False).sum()

df2v = df2.values #[list(i) for i in df2.values]






FOOD = [i for i in df2v if i[0] == 'Food']


dfFood = df2.loc[df2['TYPE'] == 'Food']
dfFoodv = dfFood.values


df2 = dfFood
df2v = dfFoodv










### Sorted alphabetically
type = sorted(set(df2['TYPE'].values))
typesum = df2.groupby(['TYPE']).sum().values

subtype = sorted(set(df2['SUBTYPE'].values))
subtypesum = df2.groupby('SUBTYPE').sum().values

subsubtype = sorted(set(df2['SUBSUBTYPE'].values))
subsubtypesum = df2.groupby('SUBSUBTYPE').sum().values

name = sorted(df2['NAME'].values)
namesum = df2.groupby('NAME').sum().values

# (category, sum price of category)
TYPE = [(i,j[0]) for i,j in zip(type,typesum)]
SUBTYPE = [(i,j[0]) for i,j in zip(subtype,subtypesum)]
SUBSUBTYPE = [(i,j[0]) for i,j in zip(subsubtype, subsubtypesum)]
NAME = [(i,j[0]) for i,j in zip(name, namesum)]
All = TYPE+SUBTYPE+SUBSUBTYPE+NAME

# [[categories],[sum prices of categories]]
# All2[0=cats,1=prices][cats,prices]
All2 = map(list, zip(*All)) # == [[i[0] for i in All],[i[1] for i in All]]


# Normalize prices to total price
# food = sum([TYPE[0][1]])/TYPE[0][1]*100
# sum(i[1] for i in TYPE)
TOTALP = sum(i[1] for i in TYPE)

All2[1] = [i/TOTALP*100. for i in All2[1]]

### Make top dir (center of sunburst plot) "Expenses"
'''
All2[0] = ['Expenses']+All2[0]
All2[1] = [100.]+All2[1]
'''

### The data structure necessary for eventual plotting
def ds(key, value, dstr=None):
    if dstr: return [(key, value, dstr)]
    else: return [(key, value, [])]
# This ds2 doesn't work...?
def ds2(t,tc, st,stc, sst,sstc, n,p):
    return ds(t, tc, ds(st, stc, ds(sst, sstc, ds(n, p))))


'''
for t,st,sst,n,p in df2v[:3]:
    tc = All2[1][All2[0].index(t)]      # type cost tc for type t
    stc = All2[1][All2[0].index(st)]    # etc
    sstc = All2[1][All2[0].index(sst)]
    print ds(t, tc, ds(st, stc, ds(sst, sstc, ds(n, p))))
    #print t,st,sst,n,p
'''


# Index in array for subdirectory/subcategory
subdir = 2
'''
# Initialize new data structure to be nested as first data. Then if there are dir matches, nest appropriately.
for i,(t,st,sst,n,p) in enumerate(df2v[:5]):
#for i,(t,st,sst,n,p) in enumerate(df2v):
    tc = All2[1][All2[0].index(t)]      # type cost tc for type t
    stc = All2[1][All2[0].index(st)]    # etc
    sstc = All2[1][All2[0].index(sst)]
    p = p/TOTALP*100 # == All2[1][All2[0].index(n)]
    print t,st,sst,n
    if i == 0:
        data = ds(t, tc, ds(st, stc, ds(sst, sstc, ds(n, p))))
    else:
        T = [d[0] for d in data]
        #ST = [d[subdir][0][0] for d in data]
        #SST = [d[subdir][0][subdir][0][0] for d in data]
        ST = [d[0] for d in data[0][subdir]]
        SST = [d[subdir][0][0] for d in data[0][subdir]]
        ########### Problem is with how SST is calc'd. Test on data0 at bottom.
        #if sst == 'Candy': print sst, n, p
        print T,ST,SST
        if t in T: # if t of current row is already in...
            if st in ST:
                if sst in SST:
                    #data[0][subdir][0][subdir][0][subdir].append(ds(n, p)[0])    
                    #data[0][subdir][SST.index(sst)][subdir][0][subdir].append(ds(n, p)[0])
                    data[0][subdir][SST.index(sst)][subdir][0][subdir].append(ds(n, p)[0])
                    #print sst
                    #else: data[0][subdir][0][subdir].append(ds(sst, sstc, ds(n, p))[0])
                    #else: data[0][subdir][ST.index(st)][subdir].append(ds(sst, sstc, ds(n, p))[0])
                else: data[0][subdir][ST.index(st)][subdir].append(ds(sst, sstc, ds(n, p))[0])
                    #print st
            else: data[0][subdir].append(ds(st, stc, ds(sst, sstc, ds(n, p)))[0])
            #print t
        else:
            data.append(ds(t, tc, ds(st, stc, ds(sst, sstc, ds(n, p))))[0])
    print ''
'''















# Initialize new data structure to be nested as first data. Then if there are dir matches, nest appropriately.
T = []
ST = []
SST = []
N = []
nt = 0 # Number in type, etc
nst = 0
nsst = 0
nn = 0
#for i,(t,st,sst,n,p) in enumerate(df2v[:8]):
for i,(t,st,sst,n,p) in enumerate(df2v):
    tc = All2[1][All2[0].index(t)]      # type cost tc for type t
    stc = All2[1][All2[0].index(st)]    # etc
    sstc = All2[1][All2[0].index(sst)]
    p = p/TOTALP*100 # == All2[1][All2[0].index(n)]
    print i,t,st,sst,n
    
    if i == 0:
        data = ds(t, tc, ds(st, stc, ds(sst, sstc, ds(n, p))))
        T.append(t)
        ST.append(st)
        SST.append(sst)
        N.append(n)
        nt += 1
        nst += 1
        nsst += 1
        nn += 1
    else:
        #T = [d[0] for d in data]
        #ST = [d[subdir][0][0] for d in data]
        #SST = [d[subdir][0][subdir][0][0] for d in data]
        #ST = [d[0] for d in data[0][subdir]]
        #SST = [d[subdir][0][0] for d in data[0][subdir]]
        ########### Problem is with how SST is calc'd. Test on data0 at bottom.
        #if sst == 'Candy': print sst, n, p
        #print T,ST,SST
        
        if t in T: # if t of current row is already in...
            print 't1'
            if st in ST:
                print 'st1'
                if sst in SST:
                    print 'sst1'
                    #data[0][subdir][0][subdir][0][subdir].append(ds(n, p)[0])    
                    #data[0][subdir][SST.index(sst)][subdir][0][subdir].append(ds(n, p)[0])
                    x = len(data[T.index(t)][subdir][ST.index(st)][subdir]) - 1
                    #data[T.index(t)][subdir][ST.index(st)][subdir][SST.index(sst)-ST.index(st)][subdir].append(ds(n, p)[0])
                    data[T.index(t)][subdir][ST.index(st)][subdir][x][subdir].append(ds(n, p)[0])

                    N.append(n)
                    nn += 1
                    #SST.append(sst)
                    #print sst
                    #else: data[0][subdir][0][subdir].append(ds(sst, sstc, ds(n, p))[0])
                    #else: data[0][subdir][ST.index(st)][subdir].append(ds(sst, sstc, ds(n, p))[0])
                else:
                    data[T.index(t)][subdir][ST.index(st)][subdir].append(ds(sst, sstc, ds(n, p))[0])
                    SST.append(sst)
                    N.append(n)
                    nn += 1
                    nsst += 1
                    #print st
            else:
                print 't2'
                data[T.index(t)][subdir].append(ds(st, stc, ds(sst, sstc, ds(n, p)))[0])
                ST.append(st)
                SST.append(sst)
                N.append(n)
                nn += 1
                nst += 1
                nsst += 1
        else:
            data.append(ds(t, tc, ds(st, stc, ds(sst, sstc, ds(n, p))))[0])
            T.append(t)
            ST.append(st)
            SST.append(sst)
            N.append(n)
            nt += 1
            nst += 1
            nsst += 1
            nn += 1
    print ''

### To clean up the above: make it only food (no T), and no p, and use eval() for dirs

'''
data = 
[('Food', 155.02000000000004, [('Bakery', 2.0, [('Croissant', 2.0, [('Croissant', 2.0, [])])]), ('Dairy', 16.740000000000002, [('Cheese', 5.4900000000000002, [('Cheese', 5.49, [])])]), ('Dairy', 16.740000000000002, [('Milk', 4.2699999999999996, [('Milk', 4.27, [])])]), ('Dairy', 16.740000000000002, [('Yogurt', 6.9800000000000004, [('Yogurt', 6.98, [])])])])]
'''
'''
for i in data[0][subdir]: i
'''





'''
        T = data[i-1][0]                  # type T in data row prior. Up to sst because n is already nondupe
        ST = data[i-1][subdir][0][0]
        SST = data[i-1][subdir][0][subdir][0][0]
        if t == T and st == ST and sst == SST:
            data[0][subdir][0][subdir][0][subdir].append(ds(n,p)[0])
            #data[0][subdir][0][subdir][0].append(ds(n, p))#data[0][subdir][0][subdir][0][subdir])
        #print ds(t, tc, ds(st, stc, ds(sst, sstc, ds(n, p))))
'''










'''
header = list(df)
header2 = header[:]
header2.remove('PRICE')
for h in header2:
    print df.groupby(h).sum()
    print ''
'''























### From https://stackoverflow.com/questions/12926779/how-to-make-a-sunburst-plot-in-r-or-python

import matplotlib.pyplot as plt

def sunburst(nodes, total=np.pi * 2, offset=0, level=0, ax=None):
    ax = ax or plt.subplot(111, projection='polar')

    if level == 0 and len(nodes) == 1:
        label, value, subnodes = nodes[0]
        ax.bar([0], [0.5], [np.pi * 2])
        ax.text(0, 0, label, ha='center', va='center')
        sunburst(subnodes, total=value, level=level + 1, ax=ax)
    elif nodes:
        d = np.pi * 2 / total
        labels = []
        widths = []
        local_offset = offset
        for label, value, subnodes in nodes:
            labels.append(label)
            widths.append(value * d)
            sunburst(subnodes, total=total, offset=local_offset,
                     level=level + 1, ax=ax)
            local_offset += value
        values = np.cumsum([offset * d] + widths[:-1])
        heights = [1] * len(nodes)
        bottoms = np.zeros(len(nodes)) + level - 0.5
        rects = ax.bar(values, heights, widths, bottoms, linewidth=1,
                       edgecolor='white', align='edge')
        for rect, label in zip(rects, labels):
            x = rect.get_x() + rect.get_width() / 2
            y = rect.get_y() + rect.get_height() / 2
            rotation = (90 + (360 - np.degrees(x) % 180)) % 360
            ax.text(x, y, label, rotation=rotation, ha='center', va='center') 

    if level == 0:
        ax.set_theta_direction(-1)
        ax.set_theta_zero_location('N')
        ax.set_axis_off()
'''
data0 = [    ('/', 100, [
                ('home', 70, [
                    ('Images', 40, []),
                    ('Videos', 20, []),
                    ('Documents', 5, []), ]),
                ('usr', 15, [
                    ('src', 6, [
                        ('linux-headers', 4, []),
                        ('virtualbox', 1, []), ]),
                    ('lib', 4, []),
                    ('share', 2, []),
                    ('bin', 1, []),
                    ('local', 1, []),
                    ('include', 1, []), ]), ]),     ]

data0 = [    ('/', 100, [('home', 70, [('Images', 40, []),('Videos', 20, []),('Documents', 5, []), ]),
                ('usr', 15, [
                    ('src', 6, [
                        ('linux-headers', 4, []),
                        ('virtualbox', 1, []), ]),
                    ('lib', 4, []),
                    ('share', 2, []),
                    ('bin', 1, []),
                    ('local', 1, []),
                    ('include', 1, []), ]), ]),     ]


sunburst(data0)
plt.show()
'''



sunburst(data)
plt.show()
