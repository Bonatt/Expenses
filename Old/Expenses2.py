import sys; sys.path.append('/home/joshua/Other/FunCode'); from ROOTStyleEtc import *





### A log of expenses starting October 31, 2017.




'''
import csv

### Header for all .log files:
#header = ['DATE', 'STORE', 'TYPE', 'SUBTYPE', 'SUBSUBTYPE', 'NAME', 'PRICE', 'PERSON']
headersize = 2
# Date of expense, e.g. 31102017
date = []
# Store of expense, e.g. Loblaws
store = []
options_store = []
# Type of expenses, e.g. Food
type = []
options_type = []
# Subtype of expenses, e.g. Produce
subtype = []
options_subtype = []
# Subsubtype of expenses, e.g. Vegetable
subsubtype = []
options_subsubtype = []
# Name of expenses, e.g. Tomatoes
name = []
options_name = []
# Price of expenses, e.g. $5.99
price = []

# All of the above...
all = []

file = 'Expenses.csv'
options_file = 'ExpensesOptions.csv'

with open(file, 'rb') as f, open(options_file, 'rb') as options_f:
    data = csv.reader(f)
    options_data = csv.reader(options_f)
    for i,(row,options_row) in enumerate(zip(data,options_data)):
        all.append(row)
        if i >= headersize:
            date.append(row[0])
            store.append(row[1])
            type.append(row[2])
            subtype.append(row[3])
            subsubtype.append(row[4])
            name.append(row[5])
            price.append(row[6])
            if len(options_row[1]) > 0: options_store.append(options_row[1])
            if len(options_row[2]) > 0: options_type.append(options_row[2])
            if len(options_row[3]) > 0: options_subtype.append(options_row[3])
            if len(options_row[4]) > 0: options_subsubtype.append(options_row[4])
            if len(options_row[5]) > 0: options_name.append(options_row[5])



# Cut header; cut date, store, person. And recaste price from string to float
a = [i[2:-1] for i in all[headersize:]]
for i in a:
    i[-1] = float(i[-1])
# Transpose of a
a2 = map(list, zip(*a))
# Uniques of a2; And recaste price from string to float
a3 = [list(set(i)) for i in a2]
#a3[-1] = [float(i) for i in a3[-1]]
# Remove price from a3
a4 = a3[:-1]
'''





import pandas as pd

file = 'Expenses.csv'
headersize = 1

df = pd.read_csv(file)
header = list(df)
header2 = header[:]
header2.remove('PRICE')
for h in header2:
    print df.groupby(h).sum()
    print ''

# Price by type. These are all alphabetical...
type = sorted(set([i for i in df['TYPE'].values[1:]]))
cost_type = [i[0] for i in df.groupby('TYPE').sum().values]
# Price by subtype
subtype = sorted(set([i for i in df['SUBTYPE'].values[1:]]))
cost_subtype = [i[0] for i in df.groupby('SUBTYPE').sum().values]
# Price by subsubtype
subsubtype = sorted(set([i for i in df['SUBSUBTYPE'].values[1:]]))
cost_subsubtype = [i[0] for i in df.groupby('SUBSUBTYPE').sum().values]
# Price by name
name = sorted(set([i for i in df['NAME'].values[1:]]))
cost_name = [i[0] for i in df.groupby('NAME').sum().values]

costs = [ (type,cost_type),(subtype,cost_subtype), (subsubtype,cost_subsubtype), (name,cost_name) ]


# (type, cost_type), etc.
tct = [(i,j) for i,j in zip(type,cost_type)]
stcst = [(i,j) for i,j in zip(subtype,cost_subtype)]
sstcsst = [(i,j) for i,j in zip(subsubtype,cost_subsubtype)]
ncn = [(i,j) for i,j in zip(name,cost_name)]

costs2 = tct+stcst+sstcsst+ncn


cats = type + subtype + subsubtype + name
costs3 = cost_type + cost_subtype + cost_subsubtype + cost_name





def dstruct(key, value, dstr=None):
    if dstr: return [(key, value, dstr)]#dstruct(dstr))]
    else: return [(key, value, [])]


ndirs = [len(type), len(subtype), len(subsubtype), len(name)]
idirs = [i-1 for i in ndirs]

TYPE = dstruct(costs2[0][0],costs2[0][1])










# Values of df into lists. Strip date, store, person, and header buffer
a = [list(i[2:-1]) for i in df.values[headersize:]]
# Transpose of a
a2 = map(list, zip(*a))
# Uniques of a2
a3 = [list(set(i)) for i in a2]


'''
aa = a[0]
#range(len(aa)-1) = [0, 1, 2, 3]
p = aa[-1]
data = dstruct(aa[0], p, dstruct(aa[1], p, dstruct(aa[2], p, dstruct(aa[3], p))))

for aa in a:
    p = aa[-1]
    dstruct(aa[0], p, dstruct(aa[1], p, dstruct(aa[2], p, dstruct(aa[3], p))))
'''

#dstruct(costs[0][0][0],costs[0][1][0], dstruct('ok2',2))



# Only type, subtype, subsubtype, name, price
header3 = header[2:-1]
# From df, take only header3 columns and drop NaN row ('' row)
df2 = df[header3].sort_values(by=header3)#[:-1]




# Only type, subtype, subsubtype, name
header4 = header[2:-2]



df3 = df.groupby(header4).sum() # == df2.groupby(header3).sum()
df4 = df.groupby(header4, as_index=False).sum()
#df.groupby(header4[::-1]).sum()




# df4 to list. == a but merges both tomatoes, e.g. No duplicate names, now.
b = [list(i) for i in df4.values]
# Uniques of b. Note that this means same prices also get collapsed...
b2 = [list(set(i)) for i in map(list, zip(*b))]

ndirs = [len(i) for i in b2[:-1]]




'''
data = []
for bb in b:
    #t = bb[0]
    #st = bb[1]
    #sst = bb[2]
    #n = bb[3] # name
    #p = bb[4] # price
    t,st,sst,n,p = bb
    #if n not in data[0][2][0][2][0][2][0]:
    data.append(dstruct(t, p, dstruct(st, p, dstruct(sst, p, dstruct(n, p)))))


# data = dstruct(bb[0], p, dstruct(bb[1], p, dstruct(bb[2], p, dstruct(bb[3], p))))
# data[0][2][0][2][0][2][0] = ('Croissant', 4.3100000000000005, []), e.g.
'''



#b0 = b[0]
data = []
for bb in b:
    t,st,sst,n,p = bb
    #tempdata = dstruct(t, p, dstruct(st, p, dstruct(sst, p, dstruct(n, p))))
    #if n not in tempdata[0][2][0][2][0][2][0]:
    #    data.append(dstruct(t, p, dstruct(st, p, dstruct(sst, p, dstruct(n, p)))))
    #else:
    data.append(dstruct(t, p, dstruct(st, p, dstruct(sst, p, dstruct(n, p)))))




'''
data = []
for cc,bb in zip(costs,b):
    tc,stc,sstc,nc = costs
    t,st,sst,n,p = bb
    data.append(dstruct(t, p, dstruct(st, p, dstruct(sst, p, dstruct(n, p)))))
'''






subdir = 2
'''
d0 = data[0][0]
d1 = data[1][0]

d0t = d0[0]
d1t = d1[0]

if d1t == d0t: d0[subdir].append(d1[subdir])
'''

'''
data2 = data[0][0] # == ('Food', 2.0, [('Bakery', 2.0, [('Croissant', 2.0, [('Croissant', 2.0, [])])])])
for i,d in enumerate(data):
    if i == 0: continue # return to top of loop, next i. To start appending at i=1
    else:
        print d[i-1]
        print d
        d0 = d[i-1]
        d1 = d
        d0t = d0[0] # "food"
        d1t = d1[0]
        if d1t == d0t: d0[subdir].append(d1[subdir])

'''
'''
# Initialize new data structure to be nested as first data. Then if there are matches, nest appropriately.
data2 = data[0][0]
for i,row in enumerate(data):
    if i == 0: continue
    else:
        #print i, row
'''

# Yes, I know this is unPythonic.
# Initialize new data structure to be nested as first data. Then if there are type matches, nest appropriately.
data2 = data[0][0]
for i in range(len(data)):
    if i == 0: continue
    else:
        d0 = data[i-1][0]   # i-1 data row
        d0t = d0[0]         # i-1 data row's type
        d1 = data[i][0]   
        d1t = d1[0]
        if d1t == d0t:
            data2[subdir].append(d1[subdir][0])
data2 = [data2]





[dstruct(k,v,[]) for k,v in zip(name,cost_name)]





























### From https://stackoverflow.com/questions/12926779/how-to-make-a-sunburst-plot-in-r-or-python

import matplotlib.pyplot as plt

def sunburst(nodes, total=np.pi * 2, offset=0, level=0, ax=None):
    ax = ax or plt.subplot(111, projection='polar')

    if level == 0 and len(nodes) == 1:
        label, value, subnodes = nodes[0]
        ax.bar([0], [0.5], [np.pi * 2])
        ax.text(0, 0, label, ha='center', va='center')
        sunburst(subnodes, total=value, level=level + 1, ax=ax)
    elif nodes:
        d = np.pi * 2 / total
        labels = []
        widths = []
        local_offset = offset
        for label, value, subnodes in nodes:
            labels.append(label)
            widths.append(value * d)
            sunburst(subnodes, total=total, offset=local_offset,
                     level=level + 1, ax=ax)
            local_offset += value
        values = np.cumsum([offset * d] + widths[:-1])
        heights = [1] * len(nodes)
        bottoms = np.zeros(len(nodes)) + level - 0.5
        rects = ax.bar(values, heights, widths, bottoms, linewidth=1,
                       edgecolor='white', align='edge')
        for rect, label in zip(rects, labels):
            x = rect.get_x() + rect.get_width() / 2
            y = rect.get_y() + rect.get_height() / 2
            rotation = (90 + (360 - np.degrees(x) % 180)) % 360
            ax.text(x, y, label, rotation=rotation, ha='center', va='center') 

    if level == 0:
        ax.set_theta_direction(-1)
        ax.set_theta_zero_location('N')
        ax.set_axis_off()
'''
data0 = [    ('/', 100, [
                ('home', 70, [
                    ('Images', 40, []),
                    ('Videos', 20, []),
                    ('Documents', 5, []), ]),
                ('usr', 15, [
                    ('src', 6, [
                        ('linux-headers', 4, []),
                        ('virtualbox', 1, []), ]),
                    ('lib', 4, []),
                    ('share', 2, []),
                    ('bin', 1, []),
                    ('local', 1, []),
                    ('include', 1, []), ]), ]),     ]

sunburst(data0)
plt.show()
'''




