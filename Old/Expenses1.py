import sys; sys.path.append('/home/joshua/Other/FunCode'); from ROOTStyleEtc import *





### A log of expenses starting October 31, 2017.





import csv

### Header for all .log files:
#header = ['DATE', 'STORE', 'TYPE', 'SUBTYPE', 'SUBSUBTYPE', 'NAME', 'PRICE', 'PERSON']
headersize = 1
# Date of expense, e.g. 31102017
date = []
# Store of expense, e.g. Loblaws
store = []
options_store = []
# Type of expenses, e.g. Food
type = []
options_type = []
# Subtype of expenses, e.g. Produce
subtype = []
options_subtype = []
# Subsubtype of expenses, e.g. Vegetable
subsubtype = []
options_subsubtype = []
# Name of expenses, e.g. Tomatoes
name = []
options_name = []
# Price of expenses, e.g. $5.99
price = []

# All of the above...
all = []

file = 'Expenses.csv'
options_file = 'ExpensesOptions.csv'

with open(file, 'rb') as f, open(options_file, 'rb') as options_f:
    data = csv.reader(f)
    options_data = csv.reader(options_f)
    for i,(row,options_row) in enumerate(zip(data,options_data)):
        all.append(row)
        if i >= headersize:
            date.append(row[0])
            store.append(row[1])
            type.append(row[2])
            subtype.append(row[3])
            subsubtype.append(row[4])
            name.append(row[5])
            price.append(row[6])
            if len(options_row[1]) > 0: options_store.append(options_row[1])
            if len(options_row[2]) > 0: options_type.append(options_row[2])
            if len(options_row[3]) > 0: options_subtype.append(options_row[3])
            if len(options_row[4]) > 0: options_subsubtype.append(options_row[4])
            if len(options_row[5]) > 0: options_name.append(options_row[5])

'''
subtype_indices = [[i for i, x in enumerate(subtype) if x == o_st] for o_st in options_subtype]
#subtype_prices = [sum(1 for [i] in price[i]) for i in subtype_indices]

for o_st in options_subtype:
    for st,p in zip(subtype,price):
        if o_st == st: 
            print o_st, st, p

'''






import pandas as pd

df = pd.read_csv(file)
header = list(df) # == df.columns.values.tolist() 
header2 = header[:]
header2.remove('PRICE')
for h in header2:
    print df.groupby(h).sum()
    print ''

# Price by name
name = sorted(set([i for i in df['NAME'].values[1:]]))
cost_name = [i[0] for i in df.groupby('NAME').sum().values]
# Price by subsubtype
subsubtype = sorted(set([i for i in df['SUBSUBTYPE'].values[1:]]))
cost_subsubtype = [i[0] for i in df.groupby('SUBSUBTYPE').sum().values]
# Price by subtype
subtype = sorted(set([i for i in df['SUBTYPE'].values[1:]]))
cost_subtype = [i[0] for i in df.groupby('SUBTYPE').sum().values]
# Price by type
type = sorted(set([i for i in df['TYPE'].values[1:]]))
cost_type = [i[0] for i in df.groupby('TYPE').sum().values]

costs = [ (type,cost_type),(subtype,cost_subtype), (subsubtype,cost_subsubtype), (name,cost_name) ]






# Sorted by SUBTYPE, then SUBSUBTYPE
dfsort = df.sort(['SUBTYPE','SUBSUBTYPE'])
dfsortv = dfsort.values


### From https://stackoverflow.com/questions/12926779/how-to-make-a-sunburst-plot-in-r-or-python

import matplotlib.pyplot as plt

def sunburst(nodes, total=np.pi * 2, offset=0, level=0, ax=None):
    ax = ax or plt.subplot(111, projection='polar')

    if level == 0 and len(nodes) == 1:
        label, value, subnodes = nodes[0]
        ax.bar([0], [0.5], [np.pi * 2])
        ax.text(0, 0, label, ha='center', va='center')
        sunburst(subnodes, total=value, level=level + 1, ax=ax)
    elif nodes:
        d = np.pi * 2 / total
        labels = []
        widths = []
        local_offset = offset
        for label, value, subnodes in nodes:
            labels.append(label)
            widths.append(value * d)
            sunburst(subnodes, total=total, offset=local_offset,
                     level=level + 1, ax=ax)
            local_offset += value
        values = np.cumsum([offset * d] + widths[:-1])
        heights = [1] * len(nodes)
        bottoms = np.zeros(len(nodes)) + level - 0.5
        rects = ax.bar(values, heights, widths, bottoms, linewidth=1,
                       edgecolor='white', align='edge')
        for rect, label in zip(rects, labels):
            x = rect.get_x() + rect.get_width() / 2
            y = rect.get_y() + rect.get_height() / 2
            rotation = (90 + (360 - np.degrees(x) % 180)) % 360
            ax.text(x, y, label, rotation=rotation, ha='center', va='center') 

    if level == 0:
        ax.set_theta_direction(-1)
        ax.set_theta_zero_location('N')
        ax.set_axis_off()

data = [    ('/', 100, [
                ('home', 70, [
                    ('Images', 40, []),
                    ('Videos', 20, []),
                    ('Documents', 5, []), ]),
                ('usr', 15, [
                    ('src', 6, [
                        ('linux-headers', 4, []),
                        ('virtualbox', 1, []), ]),
                    ('lib', 4, []),
                    ('share', 2, []),
                    ('bin', 1, []),
                    ('local', 1, []),
                    ('include', 1, []), ]), ]),     ]

sunburst(data)
#plt.show()








# Cut header; cut date, store, person. And recaste price from string to float
a = [i[2:-1] for i in all[1:]]
for i in a:
    i[-1] = float(i[-1])
# Transpose of a
a2 = map(list, zip(*a))
# Uniques of a2; And recaste price from string to float
a3 = [list(set(i)) for i in a2]
#a3[-1] = [float(i) for i in a3[-1]]
# Remove price from a3
a4 = a3[:-1]


#for i in a: i
#np.sort(a,0)

'''
for i in a: i
... 
['Food', 'Grocery', 'Sauce', 'Alfredo', 2.99]
['Food', 'Grocery', 'Sauce', 'Salsa', 3.99]
['Food', 'Grocery', 'Spice', 'Mustard', 2.79]
['Food', 'Grocery', 'Coffee', 'Coffee', 7.49]
['Food', 'Grocery', 'Grain', 'Pasta', 1.79]
['Food', 'Dairy', 'Yogurt', 'Yogurt', 2.99]
['Food', 'Dairy', 'Cheese', 'Cheese', 5.49]
['Food', 'Dairy', 'Yogurt', 'Yogurt', 3.99]
['Food', 'Grocery', 'Candy', 'Bonbon', 1.09]
['Food', 'Produce', 'Fruit', 'Banana', 0.99]
['Food', 'Produce', 'Vegetable', 'Broccoli', 2.5]
['Food', 'Produce', 'Vegetable', 'Lettuce', 1.5]
['Food', 'Produce', 'Vegetable', 'Avocado', 2.19]
['Food', 'Produce', 'Vegetable', 'Pepper', 2.96]
['Food', 'Produce', 'Vegetable', 'Tomato', 1.32]
['Food', 'Produce', 'Vegetable', 'Tomato', 2.99]
['Food', 'Meat', 'Chicken', 'Chicken', 8.97]
'''

# From the data above, we already know Alfredo is of Sauce, and Sauce is of Grocery, etc.
# So no need to create a priori definites/dictionaries/lists of what belongs where. Do it in situ.
# Put data into format from the above example. How?
#  1. Give each "directory" its own total price


'''
for i in a3: i
... 
['Food']
['Grocery', 'Produce', 'Dairy', 'Meat']
['Cheese', 'Coffee', 'Candy', 'Vegetable', 'Fruit', 'Grain', 'Yogurt', 'Chicken', 'Sauce', 'Spice']
['Bonbon', 'Cheese', 'Coffee', 'Mustard', 'Lettuce', 'Avocado', 'Pepper', 'Broccoli', 'Tomato', 'Pasta', 'Yogurt', 'Alfredo', 'Salsa', 'Chicken', 'Banana']
[2.79, 1.5, 7.49, 2.96, 5.49, 3.99, 2.99, 1.09, 8.97, 0.99, 2.19, 1.32, 1.79, 2.5]
'''







# Flatten a3, cutting price too
a3flat = [item for sublist in a3[:-1] for item in sublist]

prices = []
for s in a3flat:
    prices.append( (s,sum(float(i[-1]) for i in a if s in i)) ) #,[]) )
    print s, sum(float(i[-1]) for i in a if s in i)

### Okay, aboves gives prices for each item and category... Now how to show relationships...




d = []


def dstruct(key, value, dstr=None):
    if dstr: return [(key, value, dstruct(dstr))]
    else: return [(key, value, [])]

f = dstruct('Food', 1)
F = dstruct('Grocery',1,f)



def dstruct(key, value, dstr=None):
    if dstr: return [(key, value, dstr)]#dstruct(dstr))]
    else: return [(key, value, [])]






tp = stp = sstp = np = 0.
for t,st,sst,n,p in a:
    print t,st,sst,n,p
    #print (t,p,[st,p, [sst,[n]]])
    #print t,p,st,p,n





prices = []
for i,row in enumerate(a3):
    if i == 0: prices.append(row)






# From https://stackoverflow.com/questions/5369723/multi-level-defaultdict-with-variable-depth
from collections import defaultdict
class Tree(defaultdict):
    def __init__(self, value=None):
        super(Tree, self).__init__(Tree)
        self.value = value

r = Tree()
r.value = 1
r['a']['b'].value = 3
print r.value
print r['a']['b'].value
print r['c']['d']['f'].value

r = Tree()
for t,st,sst,n,p in a:
    #if n not
    r[t][st][sst][n].value = p









# From https://stackoverflow.com/questions/635483/what-is-the-best-way-to-implement-nested-dictionaries
class AutoVivification(dict):
    """Implementation of perl's autovivification feature."""
    def __getitem__(self, item):
        try:
            return dict.__getitem__(self, item)
        except KeyError:
            value = self[item] = type(self)()
            return value

v = AutoVivification()

for type, subtype, subsubstype, name, price in a: 
    print type, subtype, subsubstype, name, price
    v[type] = {}
    v[type][subtype] = {}
    v[type][subtype][subsubstype] = {}
    v[type][subtype][subsubstype][name] = {}
    v[type][subtype][subsubstype][name] = price




d = {}
for type, subtype, subsubtype, name, price in a:
    print type, subtype, subsubtype, name, price
    if type not in d: d[type] = {}
    if subtype not in d: d[type][subtype] = {}
    if subsubtype not in d: d[type][subtype][subsubtype] = {}
    if name not in d: d[type][subtype][subsubtype][name] = {}
    d[type][subtype][subsubtype][name] = float(price)






d = {}
for type, subtype, subsubtype, name, price in a:
    print type, subtype, subsubtype, name, price
    #t = d[type]
    #st = d[type][subtype]
    #sst = d[type][subtype][subsubtype]
    #n = d[type][subtype][subsubtype][name]
    if type not in          d:  d[type] = {}
    if subtype not in       d[type]:  d[type][subtype] = {}
    if subsubtype not in    d[type][subtype]: d[type][subtype][subsubtype] = {}
    if name not in          d[type][subtype][subsubtype]: d[type][subtype][subsubtype][name] = {}
    d[type][subtype][subsubtype][name] = float(price)







seen = set()
uniq = [x for x in a2[-2] if x not in seen and not seen.add(x)]


[x for x in a2[-2] if a2[-2].count(x) > 1]



#https://stackoverflow.com/questions/642763/find-intersection-of-two-lists/642919#642919






from collections import defaultdict
nested_dict = lambda: defaultdict(nested_dict)
nest = nested_dict()
for type, subtype, subsubstype, name, price in a:
    nest[type][subtype][subsubstype][name] = price




d = {}
for k, v in s:
    d.setdefault(k, []).append(v)



d = {}
for type, subtype, subsubstype, name, price in a:
    d.setdefault(name, []).append(price)








d = {}
d['Food'] = {}
d['Food']['Grocery'] = {}
d['Food']['Grocery']['Coffee'] = {}
d['Food']['Grocery']['Coffee'] = 2.




type = []
subtype = []
subsubtype = []
name = []
price = []

'''
# The below just does set()... huh
for row in a:
    for i,column in enumerate(row):
        if i == 0 and column not in type:
            type.append(column)
        if i == 1 and column not in subtype:
            subtype.append(column)
        if i == 2 and column not in subsubtype:
            subsubtype.append(column)
        if i == 3 and column not in name:
            name.append(column)
        if i == 4 and column not in price:
            price.append(column)
'''



price_type = []
price_subtype = []
price_subsubtype = []
price_name = []

#for i in a3:
#  for j in a:
#    if 

Type = 0
for i in a:
    if a3[0][0] in i: Type += float(i[-1]) #price_type.append(i[-1])















### Define what's what
# TYPEs_SUBTYPEs_SUBSUBTYPEs = [NAME]
Food = ['Grocery', 'Produce', 'Dairy', 'Meat']

Food_Grocery = ['Coffee', 'Candy','Grain','Sauce', 'Spice']
Food_Grocery_Grain = ['Pasta','Flour', 'Rice']
Food_Grocery_Candy = ['Chocolate','Bonbon']

Food_Produce = ['Vegetable', 'Fruit']
Food_Produce_Vegetable = ['Lettuce', 'Avocado', 'Pepper', 'Broccoli', 'Tomato']
Food_Produce_Fruit = ['Banana', 'Orange', 'Apple']


Food_Dairy = ['Cheese', 'Yogurt', 'Milk']

Food_Meat = ['Chicken', 'Pork', 'Beef']




Types = ('Food', 
         'Restaurant')
Subtypes = ('Grocery', 
            'Produce', 
            'Dairy', 
            'Meat')
'''
Subsubtypes = ( ('Coffee', 'Candy','Grain','Sauce', 'Spice'), 
                ('Vegetable', 'Fruit'),
                ('Cheese', 'Yogurt', 'Milk'),
                ('Chicken', 'Pork', 'Beef') )
'''
Subsubtypes = ( 'Coffee', 'Candy','Grain','Sauce', 'Spice',
                'Vegetable', 'Fruit',
                'Cheese', 'Yogurt', 'Milk',
                'Chicken', 'Pork', 'Beef' )
Names = ( 'Alfredo','Chocolate','Yogurt','Cheese','Bonbon','Banana','Broccoli','Lettuce','Avocado','Tomato','Pepper','Onion','Garlic','Pork','Beef','Chicken','Pasta','Mustard','Salsa','Coffee')

'''
d = {}
for t in Types:
    for st in Subtypes:
        for sst in Subsubtypes:
            
'''
















### From https://stackoverflow.com/questions/38351515/list-of-parent-and-child-into-nested-dictionary

items = {}
for parent, child in relations:
    parent_dict = items.setdefault(parent, {})
    child_dict = items.setdefault(child, {})
    if child not in parent_dict:
        parent_dict[child] = child_dict


items = {}
for type, subtype, subsubtype, name, price in a:
    type_dict = items.setdefault(type, {})
    subtype_dict = items.setdefault(subtype, {})
    #subsubtype_dict = items.setdefault(subsubtype, {})
    #name_dict = items.setdefault(name, {})
    if subtype not in type_dict:
        type_dict[subtype] = subtype_dict





















'''
d = {}

header3 = header[:]
header3.remove('DATE')
header3.remove('PERSON')

for i in header3:
    d[i] = {}
'''

d = {}
d['STORE'] = 'Loblaws','Metro','Green Fresh'
d['TYPE'] = 'Food','Bath'
d['SUBTYPE'] = 'Grocery','Dairy','Produce','Meat','Restaurant','Alcohol'








header3 = header[:]
header3.remove('DATE')
header3.remove('STORE')
header3.remove('PERSON')




# Cut header; date, store, person
a = [i[2:-1] for i in all[1:]]
# All price from string to float
for i in range(len(a)):
    a[i][-1] = float(a[i][-1])

#a2 = []
#for i in header3:
#    j = header3.index(i)

# header3 = ['TYPE', 'SUBTYPE', 'SUBSUBTYPE', 'NAME', 'PRICE']
# Transposes a
a2 = []
for h in range(len(header3)):
    a2.append([i[h] for i in a])
# Or..
a2 = np.transpose(a)


# Uniques values of each header section
a3 = []
for i in a2:
    a3.append(list(set(i)))


'''
a_type = [i[0] for i in a]
a_subtype = [i[1] for i in a]
a_subsubtype = [i[2] for i in a]
a_name = [i[3] for i in a]
a_price = [i[4] for i in a]
'''





















# Flatten a3, cutting price too
a3flat = [item for sublist in a3[:-1] for item in sublist]

for s in a3flat:
    print s, sum(float(i[-1]) for i in a if s in i)
