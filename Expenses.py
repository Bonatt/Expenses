
### A log of expenses starting October 31, 2017. See parent Expenses.ods file.

import numpy as np
import pandas as pd


file = 'Expenses.csv'

# Raw dataframe
df = pd.read_csv(file)

# Presumably the interesting data of df
df2 = df.groupby(['TYPE','SUBTYPE','SUBSUBTYPE','NAME'], as_index=False).sum()
df2v = df2.values 

### Prints out subcats nicely
df22 = df.groupby(['TYPE','SUBTYPE','SUBSUBTYPE','NAME']).sum()




### Food only. Travel, etc messes everything up. Plus, if added, it may may other things too small to read.
dfFood = df2.loc[df2['TYPE'] == 'Food']
dfFoodv = dfFood.values


### Since everything below relies on "df2(v)", I'll just rename what I need here...
df2 = dfFood
df2v = dfFoodv




### Sorted alphabetically
type = sorted(set(df2['TYPE'].values))
typesum = df2.groupby(['TYPE']).sum().values

subtype = sorted(set(df2['SUBTYPE'].values))
subtypesum = df2.groupby('SUBTYPE').sum().values

subsubtype = sorted(set(df2['SUBSUBTYPE'].values))
subsubtypesum = df2.groupby('SUBSUBTYPE').sum().values

name = sorted(df2['NAME'].values)
namesum = df2.groupby('NAME').sum().values


# [(category, sum price of category)]
TYPE = [(i,j[0]) for i,j in zip(type,typesum)]
SUBTYPE = [(i,j[0]) for i,j in zip(subtype,subtypesum)]
SUBSUBTYPE = [(i,j[0]) for i,j in zip(subsubtype, subsubtypesum)]
NAME = [(i,j[0]) for i,j in zip(name, namesum)]
All = TYPE+SUBTYPE+SUBSUBTYPE+NAME


# [[categories],[sum prices of categories]]
cat,cost = map(list, zip(*All)) # == [[i[0] for i in All],[i[1] for i in All]]

# Normalize costs to total cost. Actually, no need (at least with just Food).
#psum = float(sum(i for i in typesum))
#cost = [i/psum*100 for i in cost]






### The data structure necessary for eventual plotting
def ds(key, value, dstr=None):
    if dstr: return [(key, value, dstr)]
    else: return [(key, value, [])]






# Index in array for subdirectory of ds()
d = 2

# Initialize new data structure to be nested as first data. Then if there are dir matches, nest appropriately.
T = []
ST = []
SST = []
for i,(t,st,sst,n,p) in enumerate(df2v):
    tc = cost[cat.index(t)]      # type cost tc for type t
    stc = cost[cat.index(st)]    # etc
    sstc = cost[cat.index(sst)]
    #p = cost[cat.index(n)]
    if i == 0:
        data = ds(t, tc, ds(st, stc, ds(sst, sstc, ds(n, p))))
        T.append(t)
        ST.append(st)
        SST.append(sst)
    else:
        if t in T:
            if st in ST:
                if sst in SST:
                    x = len(data) - 1
                    y = len(data[x][d]) - 1
                    z = len(data[x][d][y][d]) - 1
                    data[x][d][y][d][z][d].append(ds(n, p)[0])
                else:
                    x = len(data) - 1
                    y = len(data[x][d]) - 1
                    data[x][d][y][d].append(ds(sst, sstc, ds(n, p))[0])
                    SST.append(sst)
            else:
                x = len(data) - 1
                data[x][d].append(ds(st, stc, ds(sst, sstc, ds(n, p)))[0])
                ST.append(st)
                SST.append(sst)
        else:
            data.append(ds(t, tc, ds(st, stc, ds(sst, sstc, ds(n, p))))[0])
            T.append(t)
            ST.append(st)
            SST.append(sst)

### To clean up the above: make it only food (no T), and no p, and use eval() for dirs?





header = list(df)
header.remove('PRICE')
header.remove('PERSON')
for h in header:
    print(df.groupby(h).sum())
    print('')























### From https://stackoverflow.com/questions/12926779/how-to-make-a-sunburst-plot-in-r-or-python
import matplotlib as mpl
import matplotlib.pyplot as plt

def sunburst(nodes, total=np.pi * 2, offset=0, level=0, ax=None):
    ax = ax or plt.subplot(111, projection='polar')

    # My additions.
    # Colors wrt to level(can't figure out wrt to directory (yet))
    # SubType Colors
    #STc = ['red', 'orange', 'cyan', 'green', 'blue', 'magenta']
    #cnames = ['r', 'orange', 'gold','g','c', 'b', 'purple']
    # ['Dairy', 'Grocery', 'Health', 'Meat', 'Produce']
    cnames = ['orange','blue', 'purple','red','green']
    #cnames = list(mpl.colors.cnames)
    #cnames = ['b', 'g', 'r', 'c', 'm', 'y', 'k']
    #cnames = [c for c in list(mpl.colors.cnames) if 'dark' in c]
    #cnames = [c for c in list(mpl.colors.cnames) if ('dark' in c) and ('grey' not in c) and ('gray' not in c)]
    #cnames = [c for c in list(mpl.colors.cnames) if ('blue' in c) and ('grey' not in c) and ('gray' not in c)]
    #cnames=plt.get_cmap('Accent')
    #st = [i for i in df2v if label in i][0][1]
    # TC[ST.index(st)]
    # Linewidth wrt to level
    #L = [3,2,1]

    if level == 0 and len(nodes) == 1:
        label, value, subnodes = nodes[0]
        ax.bar([0], [0.5], [np.pi * 2], color='white',edgecolor='white')
        ax.text(0, 0, label+'\n$'+str(round(value))[:-2], ha='center', va='center',fontsize=10, 
                #color='midnightblue', fontweight='bold',family='monospace')
                color='darkslategrey', fontweight='bold', family='monospace')
        sunburst(subnodes, total=value, level=level + 1, ax=ax)
    elif nodes:
        d = np.pi * 2 / total
        labels = []
        widths = []
        local_offset = offset
        for label, value, subnodes in nodes:
            labels.append(label)
            widths.append(value * d)
            sunburst(subnodes, total=total, offset=local_offset, level=level + 1, ax=ax)
            local_offset += value
        values = np.cumsum([offset * d] + widths[:-1])
        heights = [1] * len(nodes)
        bottoms = np.zeros(len(nodes)) + level - 0.5
        #rects = ax.bar(values, heights, widths, bottoms, linewidth=3./level, edgecolor='white', align='edge')
        rects = ax.bar(values, heights, widths, bottoms, linewidth=1, edgecolor='white', align='edge')
        for rect, label in zip(rects, labels):
            #rect.set_facecolor(STc[ST.index([i for i in df2v if label in i][0][1])])
            rect.set_facecolor(cnames[ST.index([i for i in df2v if label in i][0][1])])
            rect.set_alpha(1.-level/6.)
            x = rect.get_x() + rect.get_width() / 2
            y = rect.get_y() + rect.get_height() / 2
            rotation = (90 + (360 - np.degrees(x) % 180)) % 360
            ax.text(x, y, label, rotation=rotation, ha='center', va='center',fontsize=7, color='white',
                    family='monospace') 

    if level == 0:
        ax.set_theta_direction(-1)
        ax.set_theta_zero_location('N')
        ax.set_axis_off()

'''
data = [    ('/', 100, [
                ('home', 70, [
                    ('Images', 40, []),
                    ('Videos', 20, []),
                    ('Documents', 5, []), ]),
                ('usr', 15, [
                    ('src', 6, [
                        ('linux-headers', 4, []),
                        ('virtualbox', 1, []), ]),
                    ('lib', 4, []),
                    ('share', 2, []),
                    ('bin', 1, []),
                    ('local', 1, []),
                    ('include', 1, []), ]), ]),     ]

sunburst(data)
plt.show()
'''

'''
font = {'family' : 'small-caps',
        'weight' : 'bold',
        'size'   : 12}
plt.rc('font', **font)
plt.rcParams["font.family"] = "sans-serif"
'''


sunburst(data)
plt.savefig('Food.png')
#plt.savefig('Food.pdf')
plt.show()
