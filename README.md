### A log of expenses starting October 31, 2017

This was a short-lived project (barely lasted a month, oops) to gain a better understanding of where my money was being spent.
Given purchases logged within an MXL/ODS and exported to CSV, (my and my girlfriend's) food expenses were filled into a 
data structure semi-recursively 
and visualized in sunburst plot. 
Since 99% of my purchases were food, I only created a plot for food.

The data looks like this:

```
>>> df.head()
         DATE    STORE  TYPE  SUBTYPE SUBSUBTYPE         NAME  PRICE  PERSON
0  2017-10-31  Loblaws  Food  Grocery      Sauce      Alfredo   2.99    Both
1  2017-10-31  Loblaws  Food  Grocery      Sauce        Salsa   3.99    Both
2  2017-10-31  Loblaws  Food  Grocery      Spice      Mustard   2.79    Both
3  2017-10-31  Loblaws  Food  Grocery     Coffee  Tim Hortons   7.49  Joshua
4  2017-10-31  Loblaws  Food  Grocery      Grain        Pasta   1.79    Both
```

![Expenses](Food.png)



Below are outputs of the full dataset in some different formats:

```
                                         PRICE
TYPE   SUBTYPE SUBSUBTYPE  NAME
Bath   Hair    Conditioner Conditioner    5.99
Food   Dairy   Butter      Butter         3.97
               Cheese      Block          5.49
               Eggs        Eggs           7.47
               Milk        1.00%         12.81
                           Buttermilk     4.89
                           Powdered      29.99
               Yogurt      Flavored      12.96
                           Plain          6.78
       Grocery Baked       Brownies       8.00
                           Croissant      4.00
               Candy       Bonbon         1.09
                           Chocolate     27.98
               Coffee      Instant        2.99
                           Tim Hortons   14.98
               Grain       Chickpeas      1.59
                           Oat            3.19
                           Pasta          3.48
               Nuts        Pinenuts      12.49
               Other       Gelatin        7.49
               Sauce       Alfredo        8.97
                           Hummus         7.00
                           PeanutButter   4.99
                           Salsa          3.99
               Seeds       Flax           1.46
                           Sunflower      1.52
               Spice       Cinnamon       4.29
                           Mustard        2.79
               Sugar       Granulated     2.49
       Health  Protein     Whey          54.69
       Meat    Beef        Ground        28.98
                           TopSirloin    19.23
               Chicken     Breast        23.48
               Pork        Loin          17.05
                           Peameal        7.49
               Tofu        Tofu           3.99
       Produce Fruit       Banana         5.83
                           Kiwi           2.96
                           Pomegranate    6.00
                           Raisin         6.99
               Vegetable   Avocado        2.19
                           Broccoli       2.50
                           Carrot         3.99
                           Celeryroot     4.52
                           Cucumber       0.99
                           Eggplant       3.79
                           Garlic         1.29
                           Ginger         0.40
                           GreenOnion     1.98
                           Leeks          3.99
                           Lettuce        2.99
                           Mushroom       2.79
                           Onion          1.79
                           Pepper         8.93
                           Potatoes       2.94
                           Pumpkin        3.89
                           Tomato        11.05
Travel Bus     Presto      OCTranspo     33.75
```

```
              PRICE
STORE
Green Fresh    3.99
Internet      88.44
Loblaws      393.16
```
```
         PRICE
TYPE
Bath      5.99
Food    445.85
Travel   33.75
```
```
          PRICE
SUBTYPE
Bus       33.75
Dairy     84.36
Grocery  124.78
Hair       5.99
Health    54.69
Meat     100.22
Produce   81.80
```
```
             PRICE
SUBSUBTYPE
Baked        12.00
Beef         48.21
Butter        3.97
Candy        29.07
Cheese        5.49
Chicken      23.48
Coffee       17.97
Conditioner   5.99
Eggs          7.47
Fruit        21.78
Grain         8.26
Milk         47.69
Nuts         12.49
Other         7.49
Pork         24.54
Presto       33.75
Protein      54.69
Sauce        24.95
Seeds         2.98
Spice         7.08
Sugar         2.49
Tofu          3.99
Vegetable    60.02
Yogurt       19.74
```
